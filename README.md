Our Mission:
Is to raise young leaders that will change society

Our Vision: 
Is to empower youth to discover and nurture their gifts and talents; compel them to explore their interest, and conceptualize their challenges and to encourage them to youth defy their fears and create new paradigms. 

Our goal:
Is to develop young leaders, equipping them with the necessary tools &amp; insights to soar towards amazing heights of creativity and innovativeness that will transform their society

Values
 A burden to save youth lives 
A passion to empower youth 
A drive to show youth we care 
High demand for raising leaders.

Website : https://theedreamcenter.org
